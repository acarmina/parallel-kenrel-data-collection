#!/bin/bash

out=$(./subtask.sh $1 $2)

COUNT=$(flock -w 10 -x count_$2 ./cinc.sh count_$2 );
echo -e $out
echo "task $$ is done, worker $WORKER terminated,  conter restored at $COUNT" 1>&2
